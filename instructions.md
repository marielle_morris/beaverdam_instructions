*** Please use Desktop - videos may not load properly on mobile devices

## Welcome to our Video Attribute Annotation Tool

Please watch the video and use the checkboxes and text box to describe the person or animal in the shaded box. 

## Quick Instructions

1. Watch video-- pay attention to person/animal, but also consider context

2. Look over traits on right

3. Select applicable traits to the person/animal

4. If fewer than *five* apply, write your own traits in the text box

5. If you have any comments on how to improve our HIT, please write them following an * in the text box. 


Please make your descriptions specific. Try to avoid the obvious, such as "large" when the object is an elephant. 


  


**If you have any issues with this HIT, please email mlm13@rice.edu. Thanks!! 




